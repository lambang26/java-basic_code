/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.tutorial_java_niomix;

/**
 *
 * @author corona
 */
public class Variabel {
    
    public static void main (String args[]){
    //- Diawali dengan :
    //huruf/abjad
    // karaktter mata uang ($)
    //underscore(_)
    
//        int a =  10;
//        float _b = 4;
//        boolean $c =true;
//        
        int tanggalLahir  =27;
        float sepuluhDibagiTiga =3.33f;
        boolean iniBenar = true;
        char hurufA = 65;
        
//        System.out.println("variabel ini = " + a);
//        System.out.println("variabel b = " + _b);
//        System.out.println("variabel c = " + $c);

       System.out.println(tanggalLahir);
       System.out.println(sepuluhDibagiTiga);
       System.out.println(iniBenar);
       System.out.println(hurufA);
       System.out.println("helloword.toUpperCase");      
    }
    
}
