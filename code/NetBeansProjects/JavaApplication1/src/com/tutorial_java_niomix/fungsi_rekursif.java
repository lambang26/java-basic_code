/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.tutorial_java_niomix;

/**
 *
 * @author corona
 */
public class fungsi_rekursif {
    public void tampilString(int sisa){
        if(sisa>0){
            System.out.println("Belajar Rekursif java");
            tampilString(sisa-1);
        }
    }
    public static void main(String args[]){
        fungsi_rekursif r1=new fungsi_rekursif();
        r1.tampilString(5);
    }
}
