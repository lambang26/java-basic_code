/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.tutorial_java_niomix;

/**
 *
 * @author corona
 */
import java.util.Arrays;

public class arraySort{
   public static void main (String [] args){

      char arrA [] = {'d','c','b','a'};

      System.out.print("Array sebelum diurutkan: " + 
                        String.valueOf(arrA) +"\n");
      //mengurutkan isi array
      Arrays.sort(arrA);

      System.out.print("Array setelah diurutkan: " + 
      String.valueOf(arrA)); 
    }
}