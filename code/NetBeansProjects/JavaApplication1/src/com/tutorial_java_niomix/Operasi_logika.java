/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.tutorial_java_niomix;

/**
 *
 * @author corona
 */
public class Operasi_logika {
     public static void main(String args[]){
   
        System.out.println(a && b); // AND
        System.out.println(a & b);  // bolean logika AND
        System.out.println(a || b); // OR nama lain atau
        System.out.println(a | b);  // bolean logika inclusive OR
        System.out.println(a ^ b);  // bolean logika exnclusive OR atau XOR
        System.out.println(a ! b);  // logika NOT
  
        }
}
