/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.tutorial_java_niomix;
import java.util.Scanner;
/**
 *
 * @author corona
 */
 

 Public class BelajarInput {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Masukkan angka tertentu : ");
        int angkaDariUser = scanner.nextInt();
       
        System.out.println("Nilainya adalah " + angkaDariUser);
        scanner.close();
    }
}
