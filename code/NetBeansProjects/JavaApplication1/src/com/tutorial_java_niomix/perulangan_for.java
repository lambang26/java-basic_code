/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.tutorial_java_niomix;

/**
 *
 * @author corona
 */
public class perulangan_for {
    public static void main(String[] args) {
       int a = 15;
       for (int b = 1; b<=a; b++){
           for (int c = 4; c >= b; c--) {
               System.out.print(' ');
           }
           for (int d = 1; d <= b; d++){
               System.out.print('*');
           }
           System.out.println();
       }
    }
}
