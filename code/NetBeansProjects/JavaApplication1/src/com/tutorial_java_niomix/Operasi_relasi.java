/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.tutorial_java_niomix;

/**
 *
 * @author corona
 */
public class Operasi_relasi {
    public static void main(String args[]){
        
        int a = 4;
        int b = 3;
        
        System.out.println(a==b);
        System.out.println(a != b);
        System.out.println(a  < b);
        System.out.println(a  > b);
        System.out.println(a  >= b);
        System.out.println(a   <= b);
        
    }
    
}
