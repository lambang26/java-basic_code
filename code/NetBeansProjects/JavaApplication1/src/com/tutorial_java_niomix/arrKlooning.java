/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.tutorial_java_niomix;

/**
 *
 * @author corona
 */
public class arrKlooning {
     public static void main (String [] args){
        //array asal
        char arrAsal[] = {'W','O','R','L','D'};
        //array hasil kloning
        char arrClone[] = arrAsal.clone();
        
        System.out.println(String.valueOf(arrClone));
    }
}
