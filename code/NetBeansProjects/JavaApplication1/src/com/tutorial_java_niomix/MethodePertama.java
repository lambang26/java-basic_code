/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.tutorial_java_niomix;

/**
 *
 * @author corona
 */
public class MethodePertama {
	public static int absolute(int angka) {
        if (angka < 0) {
            return angka * -1;
        } else {
            return angka;
        }
    }
}
