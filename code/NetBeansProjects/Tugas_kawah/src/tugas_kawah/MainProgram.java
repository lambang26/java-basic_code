/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tugas_kawah;

/**
 *
 * @author corona
 */
public class MainProgram {
  	public static void main(String[] args) {
	
		Kendaraan mobil = new Kendaraan(6);

		mobil.tancapGas();
		
		Kendaraan motor = new Kendaraan();
		
		Kendaraan mobilLain = new Kendaraan(6, "Apapun", "D 1234 ABC");

		mobilLain.setMerekKendaraan("Merek lain");

		System.out.println(mobilLain.getMerekKendaraan());

	}
}
