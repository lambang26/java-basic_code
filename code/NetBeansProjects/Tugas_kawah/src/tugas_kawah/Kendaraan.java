/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tugas_kawah;

/**
 *
 * @author corona
 */
public class Kendaraan {
	
	public int jumlahRoda;
	private String merekKendaraan;
	protected String platNomor;
	public static int jarakMaksimal = 100;
	
	public Kendaraan() {
	
		jumlahRoda = 4;
		tancapGas();
	
	}
	
	public Kendaraan(int varA) {
	
		jumlahRoda = varA;
	
	}
	
	public Kendaraan(int jumlahRoda, String merekKendaraan, String platNomor){
	
		this.jumlahRoda = jumlahRoda;
		this.merekKendaraan = merekKendaraan;
		this.platNomor = platNomor;
	
	}
	
	public void tancapGas() {
	
		System.out.println("Kendaraan beroda " + jumlahRoda + " berjalan");
	
	}

	public void cetakPlatNomor() {
	
		System.out.println(platNomor);
	
	}
	
	void apaMerekKendaraan() {
	
		System.out.println(merekKendaraan);
	
	}
	
	public String getMerekKendaraan() {
	
		return merekKendaraan;
		
	}
	
	public void setMerekKendaraan(String merekBaru) {
	
		this.merekKendaraan = merekBaru;
	
	}
	
	public static void berapaJarakMaksimal() {
	
		System.out.println(jarakMaksimal);
	
	}
	
}