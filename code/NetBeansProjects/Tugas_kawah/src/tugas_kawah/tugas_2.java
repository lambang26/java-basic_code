/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tugas_kawah;
import java.util.Scanner; //IMPORT PACKAGE INPUTAN scanner
/**
 *
 * @author corona
 */
public class tugas_2 {
    public static void main(String args[]){
        Scanner menu = new Scanner (System.in);
        boolean islanjut = true; ///membuat declarasi booelean agar bisa next ke pilihan selanjutnya
        while(islanjut){ // pilihan ketika hasilnya sudah muncul akan melanjutkan ke step selanjutnya atau tidak
        ///Cara kerja perulangan ini seperti percabangan, ia akan melakukan perulangan selama kondisinya bernilai true . Penjelasan: kondisi bisa kita isi dengan perbandingan maupun variabel boolean
        String PilihMenu; // DECLARASI MENU STRING
        //mengenalkan variabel ke program dan menentukan tipe datanya yang saya pakai yaitu String
        System.out.println("********** SELAMAT DATANG ***********"); //menu
        System.out.println("1.\tNilai Absolute (mutlak)");
        System.out.println("2.\tNilai Terbesar Angka");
	System.out.println("3.\tPerpangkatan Angka");
	//print_pilihan_user	
        System.out.println("====================================");
        System.out.print("Pilihan [1/2/3] = "); //INPUTAN USER
	int UserPilih =menu.nextInt(); // deklarasi di sini yaitu mengenalkan variabel ke program dan menentukan tipe datanya yang saya pakai yaitu int
        switch(UserPilih){
			case 1:
				System.out.println("Nilai Absolute");//mengubah nilai negatif - ke positif +, dan nilai positif + tetap positif +
				Scanner number;//declarasi number
		
				int a ; // ASSIGNMENT Proses assignment adalah proses pemberian nilai kepada suatu variabel yang telah dideklarasikan
				number = new Scanner(System.in); //fungsi untuk menginputkan data / nilai saat setelah program di running/di jalankan.
		
				System.out.print("Masukan Variabel Angka = ");  // Masukan dari user
				a = number.nextInt(); //perintah untuk memasukan nilai .nextInt() dugunakan untuk type Integer
                                
				a = (a < 0 ? - a : a); //Simbolnya menggunakan tanda tanya (?) dan titik-dua (:) untuk memisah jawabannya.
				// Pada contoh di atas, adalah pertanyaan atau kondisi
                                //Jika a < 0 bernilai true maka yg diambil nilai -a, jika bernilai false maka yg diambil nilai a.
                                 System.out.println("a = " + a);
                                break;
			case 2:
				System.out.println("Nilai Terbesar");
				Integer array[] = {-5,100,1,-14,14,2,28,2,11,2,-100}; //membuat array nilai terbesar dalam kumpulan angka 
                Integer indexTerbesar = array[0];
                 for(int i=0;i<array.length;i++){    //menentuksn nilai terbesar dengan rumus length fungsi ini digunakan untuk memerikasa panjang dari variabel dengan tipe data string
                	 if(indexTerbesar < array[i]){
                                indexTerbesar = array[i];
                        }      
                 } System.out.println("indexTerbesar = " + indexTerbesar); //cetak nilai terbesar
                break;
			case 3:
			System.out.println("Perpangkatan");
			int hasil = 1; 
		        int angka, pangkat; 
		        Scanner b=new Scanner(System.in);
		        System.out.print("Masukkan angka: ");
		        angka=b.nextInt();
		  
		        System.out.print("Masukkan pangkat: ");
		        Scanner c=new Scanner(System.in);
		        pangkat=c.nextInt();

		        for(int i=1;i<=pangkat;i++){
		            hasil=hasil*angka; 
		        }
		        System.out.println( "Hasil "+angka+" pangkat "+pangkat+" = " +hasil);
		        break;
        default:
            System.err.println("\nMaaf input tidak di kenali");// jika user menginput tidak ada angka 1-3 akan muncul eror merah
            break;
        }
        System.out.println("====================================");
        islanjut = pengulangan("apakah anda ingin melanjutkan"); ///setelah muncul hasil akan ada pertanyaaan
        }
    }
         private static boolean pengulangan(String mesage){
     
        Scanner input = new Scanner(System.in);
        System.out.print("\n" + mesage + "(y/n)");
        String data = input.nextLine();
        
        while(!data.equalsIgnoreCase("y") && !data.equalsIgnoreCase("n")){
            System.err.println("Maaf input tidak dikenali");
            System.out.print("\n" + mesage + "(y/n)");
            data = input.nextLine();
        }
        
        return data.equalsIgnoreCase("y");
   
    }
}