/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package latihan;
import java.util.Scanner;
/**
 *
 * @author corona
 */
public class helloword {
         public static void main(String[] args) {
        // Jika user memilih menu perbandingan, maka user akan diminta untuk memasukkan dua buah bilangan, lalu akan tercetak ke layar sesuai dari hasil perbandingan dua bilangan tersebut. Misal 27 lebih besar dari 20, 20 lebih kecil dari 25, 20 sama dengan 20 lalu akan keluar dari program.
        System.out.println("Pilih menu perbandingan");
        System.out.println("1. Lebih besar");
        System.out.println("2. Lebih kecil");
        System.out.println("3. Sama dengan");
        System.out.println("4. Keluar");
        System.out.println("Masukkan pilihan Anda: ");
       
        int pilihan = scanner.nextInt();
       
        if (pilihan == 1) {
            System.out.println("Masukkan dua buah bilangan: ");
            int bilangan1 = scanner.nextInt();
            int bilangan2 = scanner.nextInt();
            if (bilangan1 > bilangan2) {
                System.out.println(bilangan1 + " lebih besar dari " + bilangan2);
            } else {
                System.out.println(bilangan1 + " lebih kecil dari " + bilangan2);
            }
        } else if (pilihan == 2) {
            System.out.println("Masukkan dua buah bilangan: ");
            int bilangan1 = scanner.nextInt();
            int bilangan2 = scanner.nextInt();
            if (bilangan1 < bilangan2) {
                System.out.println(bilangan1 + " lebih kecil dari " + bilangan2);
            } else {
                System.out.println(bilangan1 + " lebih besar dari " + bilangan2);
            }
        } else if (pilihan == 3) {
            System.out.println("Masukkan dua buah bilangan: ");
            int bilangan1 = scanner.nextInt();
            int bilangan2 = scanner.nextInt();
            if (bilangan1 == bilangan2) {
                System.out.println(bilangan1 + " sama dengan " + bilangan2);
            } else {
                System.out.println(bilangan1 + " tidak sama dengan " + bilangan2);
            }
        } else if (pilihan == 4) {
            System.out.println("Terima kasih telah menggunakan program ini");
        } else {
            System.out.println("Pilihan Anda tidak tersedia");
        }
    }
}
